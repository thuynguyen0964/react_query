import { useState } from 'react';
type Props = {
  editTodo: (value: string, id: string) => void;
  task: any;
};

export const EditTodoForm = ({ editTodo, task }: Props) => {
  const [value, setValue] = useState(task.task);

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    editTodo(value, task.id);
  };

  return (
    <form onSubmit={handleSubmit} className='TodoForm'>
      <input
        type='text'
        value={value}
        onChange={(e) => setValue(e.target.value)}
        className='todo-input'
        placeholder='Update task'
      />
      <button type='submit' className='todo-btn'>
        Add Task
      </button>
    </form>
  );
};
