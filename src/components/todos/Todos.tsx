type Props = {
  todo: ITodo;
  deleteTodo: (id: string) => void;
  editTodo: (id: string) => void;
  completeTodos: (todo: ITodo) => void;
};

export const Todo = ({ todo, deleteTodo, editTodo, completeTodos }: Props) => {
  return (
    <div className='Todo'>
      <p
        style={{ userSelect: 'none' }}
        className={`${todo.completed ? 'completed' : ''}`}
        onClick={() => completeTodos(todo)}
      >
        {todo.title}
      </p>
      <div className='flex'>
        <button onClick={() => editTodo(todo.id)}>Edit</button>
        <button onClick={() => deleteTodo(todo.id)}>Remove</button>
      </div>
    </div>
  );
};
