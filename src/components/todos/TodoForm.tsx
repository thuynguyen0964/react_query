import { useState } from 'react';
import { uuid } from '../../utils/contants';

type Props = {
  addTodo?: (todo: ITodo) => void;
};

export const TodoForm = ({ addTodo }: Props) => {
  const [value, setValue] = useState('');

  const newTodo = {
    id: uuid(),
    title: '',
    completed: false,
    isEditing: false,
  };
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    addTodo && addTodo({ ...newTodo, title: value });
    setValue('');
  };
  return (
    <form onSubmit={handleSubmit} className='TodoForm'>
      <input
        type='text'
        value={value}
        onChange={(e) => setValue(e.target.value)}
        className='todo-input'
        placeholder='What is the task today?'
      />
      <button type='submit' className='todo-btn'>
        Add Task
      </button>
    </form>
  );
};
