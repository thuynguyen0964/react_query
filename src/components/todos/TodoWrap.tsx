import { EditTodoForm } from './EditTodo';
import { Todo } from './Todos';
import { TodoForm } from './TodoForm';
import { useQuery, useQueryClient, useMutation } from '@tanstack/react-query';
import {
  addTodo,
  completeTodos,
  deleteTodo,
  getAllTodos,
} from '../../services/todos';
import { useState } from 'react';

const PAGE = 1;
const LIMIT = 5;
type Status = 'next' | 'prev';

export const TodoWrapper = () => {
  const queryClient = useQueryClient();
  const [page, setPage] = useState<number>(PAGE);

  const { data, isError, isLoading } = useQuery<ITodo[]>({
    queryKey: ['todos', page],
    queryFn: () => getAllTodos(LIMIT, page),
    keepPreviousData: true,
    staleTime: 1 * 60 * 1000,
  });

  const addMutation = useMutation({
    mutationFn: (todo: ITodo) => addTodo(todo),
    onSuccess: () => {
      queryClient.invalidateQueries(['todos']);
    },
  });

  const deleteMutation = useMutation({
    mutationFn: (id: string) => deleteTodo(id),
    onSuccess: () => {
      queryClient.invalidateQueries(['todos']);
    },
  });

  const paginateTodo = (status: Status) => {
    switch (status) {
      case 'next':
        return setPage((prev) => prev + 1);
      default:
        return setPage((prev) => prev - 1);
    }
  };

  const completeTodo = useMutation({
    mutationFn: (todo: ITodo) => completeTodos(todo),
    onSuccess: () => {
      queryClient.invalidateQueries(['todos']);
    },
  });

  const editTodo = (id: string) => null;

  const editTask = (task: string, id: string) => null;

  return (
    <div className='TodoWrapper'>
      <h1>Get Things Done !</h1>
      <TodoForm addTodo={addMutation.mutate} />

      {data &&
        data?.length > 0 &&
        data.map((todo) =>
          todo.isEditing ? (
            <EditTodoForm key={todo.id} editTodo={editTask} task={todo} />
          ) : (
            <Todo
              key={todo.id}
              todo={todo}
              deleteTodo={deleteMutation.mutate}
              editTodo={editTodo}
              completeTodos={completeTodo.mutate}
            />
          )
        )}

      {isLoading ? (
        <p className='white'>Loading todos....</p>
      ) : isError ? (
        <p className='white'>Have some error...</p>
      ) : (
        ''
      )}

      <div className='flex paginate'>
        <button disabled={page === 1} onClick={() => paginateTodo('prev')}>
          Prev
        </button>
        <button onClick={() => paginateTodo('next')}>Next</button>
      </div>
    </div>
  );
};
