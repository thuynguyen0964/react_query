import api from '../api/config';

const addTodo = async (todo: ITodo) => {
  const res = await api.post<ITodo>('/todos', { ...todo, isEditing: false });
  return res.data;
};

const getAllTodos = async (limit: number, page: number) => {
  const res = await api.get(`todos?_page=${page}&_limit=${limit}`);
  return res.data;
};

const deleteTodo = async (id: string | number) => {
  await api.delete(`todos/${id}`);
};

const completeTodos = async (todo: ITodo) => {
  const res = await api.patch(`todos/${todo.id}`, {
    ...todo,
    completed: !todo.completed,
  });
  return res.data;
};

export { addTodo, getAllTodos, deleteTodo, completeTodos };
