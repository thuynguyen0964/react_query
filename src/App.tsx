import TodoWrapper from './components/todos';

function App() {
  return (
    <section className='App'>
      <TodoWrapper />
    </section>
  );
}

export default App;
